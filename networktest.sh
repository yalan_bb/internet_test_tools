#!/bin/bash
serverlistpingtest=( "1.1.1.1" "8.8.8.8" "9.9.9.9" "quicksight.ap-south-1.amazonaws.com" "quicksight.ap-southeast-1.amazonaws.com" "quicksight.us-west-2.amazonaws.com" "quicksight.eu-west-2.amazonaws.com" )
serverlistspeedtest=( "38900" "24683" "16671" "7379" "18976" "6432" "36627" "11558" )

printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ >| PING TEST |< ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
printf "%-50s %-10s %-10s %-10s %-10s\n" "Destination" "Avg (ms)" "Max (ms)" "Min (ms)" "Packet loss"
for i in "${serverlistpingtest[@]}"
do
    output=$(ping -c 10 $i)
    destination="$i"
    allping=$(echo "$output" | grep -oP '(?<=mdev \= ).*(?=\/)')
    minping="$(echo "$allping" | cut -d '/' -f1)"
    avgping="$(echo "$allping" | cut -d '/' -f2)"
    maxping="$(echo "$allping" | cut -d '/' -f3)"
    packetloss=$(echo "$output" | grep -oP '(?<=received, ).*(?= packet loss)')
    printf "%-50s %-10s %-10s %-10s %-10s\n" "$destination" "$avgping" "$maxping" "$minping" "$packetloss"

done

printf "\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ >| SPEEDTEST TEST |< ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
printf "%-64s %-10s %-16s %-16s\n" "Server" "Ping" "Download Speed" "Upload Speed"
for i in "${serverlistspeedtest[@]}"
do
    output=$(speedtest-cli --server $i)
    host=$(echo "$output" | grep -oP '(?<=Hosted by ).*(?= \[)')
    download=$(echo "$output" | grep -oP '(?<=Download: ).*\/s')
    upload=$(echo "$output" | grep -oP '(?<=Upload: ).*\/s')
    ping=$(echo "$output" | grep -oP '(?<=: ).*ms')
    printf "%-64s %-10s %-16s %-16s\n" "$host" "$ping" "$download" "$upload"
done