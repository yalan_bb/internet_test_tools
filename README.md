## General Info
A bunch of internet ping and speed checker bash scripts

## Technologies
* bash
* speedtest-cli

## Setup
* To run networktest.sh
```
sudo pacman -S speedtest-cli
```
or
```
sudo apt-get install speedtest-cli
```
then
```
git clone https://bitbucket.org/yalan_bb/internet_test_tools.git
cd ./internet_test_tools
./networktest.sh
```
